{ pkgs, modulesPath, lib, ... }:
with lib;
let
  flux-core = pkgs.nur.repos.rseops.flux-core;
  flux-sched = pkgs.nur.repos.rseops.flux-sched;
  # pythonEnv = pkgs.python310.withPackages (p: with p; [
  #   cairocffi
  # ]);
in
{
  roles = {
    foo = { pkgs, ... }:
      {
        environment.systemPackages = [ flux-core flux-sched ];
        nixpkgs.overlays = [
          (final: prev: {
            flux-core = prev.flux-core.overrideAttrs (old: {
              buildInputs = old.buildInputs ++ [ pkgs.cairo ];
            });
          })
        ];
      };
  };
  testScript = ''
    with subtest("can_start_flux"):
        foo.succeed("flux start --test-size=4")
        foo.succeed("flux resource status")

    with subtest("launch_work"):
        foo.succeed("flux exec flux getattr rank | grep 3")

  '';
}
