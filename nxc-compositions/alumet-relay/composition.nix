{ pkgs, lib, ... }:
let
  configAlumet = {
    imports = [ ../../modules/services/alumet/alumet.nix ]; 
    environment.systemPackages = with pkgs; [ vim curl nixos-firewall-tool iptables ];
  };

  # To change package used
  newPackage = pkgs.callPackage ../../pkgs/alumet-without-csv/default.nix {};
 
in
{
  roles = {
    server = { pkgs, ... }:
    {
      imports = [ configAlumet ];
      environment.systemPackages = with pkgs; [influxdb2-cli ];

      services.alumet = {
        package = newPackage;

        server = {
          enable = true;
          address = "::";
        };

        # Test config options
        influxDbPlugin = {
          enable = true;
        };

#        csvPlugin.enable = true;
      };

      systemd.services.influxdb2.serviceConfig.RestartSec = 6000;

      services.influxdb2.enable = true;

      services.influxdb2.provision = {
        enable = true;
        initialSetup = {
          organization = "default";
          bucket = "default";
          passwordFile = pkgs.writeText "admin-pw" "password";
          tokenFile = pkgs.writeText "admin-token" "token";
        };
        organizations.someorg = {
          buckets.somebucket = {};
          auths.sometoken = {
            description = "some auth token";
            readBuckets = ["somebucket"];
            writeBuckets = ["somebucket"];
          };
        };
        users.someuser.passwordFile = pkgs.writeText "tmp-pw" "password";
      };
    };

    client = { pkgs, ... }:
    {
      imports = [ configAlumet ];


      services.alumet = {

        client.enable = true;
        client.name = "client1";
        client.collectorUri = "http://server:50051";

        raplPlugin.enable = true;
      };
    };
  };
  testScript = ''
    server.wait_for_unit("influxdb2.service")
    server.wait_for_unit("alumet-server.service")

    client.wait_for_unit("alumet-client.service")

    with subtest("getInfluxDbValues"):
        server.succeed("influx config create -a -n test -u http://localhost:8080 -t token -o default")
        server.succeed("influx query 'from(bucket:"default") |> range(start:-1m)'")
  '';
}
