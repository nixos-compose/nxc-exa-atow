{ pkgs, modulesPath, ... }:
let
  nbNodes = 2;
  nbNodesStr = builtins.toString nbNodes;
in
{
  roles =
    let
      tokenFile = pkgs.writeText "token" "p@s$w0rd";
      slurmConfig = import ./slurm_config.nix { inherit pkgs modulesPath; nbNodes = nbNodesStr; };
    in
    {
      frontend = { ... }: {
        imports = [ slurmConfig ];
        nxc.sharedDirs."/users".server = "server";

        services.slurm.enableStools = true;
      };

      server = { ... }: {
        imports = [ slurmConfig ];
        nxc.sharedDirs."/users".export = true;

        services.slurm.server.enable = true;
        services.slurm.dbdserver.enable = true;
      };

      node = { ... }: {
        imports = [ slurmConfig ];
        nxc.sharedDirs."/users".server = "server";

        services.slurm.client.enable = true;
      };

    };

  rolesDistribution = { node = nbNodes; };

  testScript = ''
    start_all()

    # Make sure DBD is up after DB initialzation
    with subtest("can_start_slurmdbd"):
        server.succeed("systemctl restart slurmdbd")
        server.wait_for_unit("slurmdbd.service")
        server.wait_for_open_port(6819)

    with subtest("can_start_slurmctld"):
        server.succeed("systemctl restart slurmctld")
        server.wait_for_unit("slurmctld.service")

    with subtest("can_start_slurmd"):
        node1.succeed("systemctl restart slurmd.service")
        node1.wait_for_unit("slurmd")
  '';
}
