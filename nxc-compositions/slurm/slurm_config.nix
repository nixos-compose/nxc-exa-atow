{ pkgs, nbNodes, modulesPath }:
let
  passFile = pkgs.writeText "dbdpassword" "password123";
  inherit (import "${toString modulesPath}/tests/ssh-keys.nix" pkgs)
    snakeOilPrivateKey snakeOilPublicKey;
in
{
  environment.systemPackages = [
    pkgs.python3
    pkgs.vim
    pkgs.cpufrequtils
    pkgs.python3Packages.clustershell
    pkgs.htop
    pkgs.tree
  ];

  # Allow root yo use open-mpi
  environment.variables.OMPI_ALLOW_RUN_AS_ROOT = "1";
  environment.variables.OMPI_ALLOW_RUN_AS_ROOT_CONFIRM = "1";

  nxc.users = { names = [ "user1" "user2" ]; prefixHome = "/users"; };

  security.pam.loginLimits = [
    { domain = "*"; item = "memlock"; type = "-"; value = "unlimited"; }
    { domain = "*"; item = "stack"; type = "-"; value = "unlimited"; }
  ];

  environment.etc."privkey.snakeoil" = {
    mode = "0600";
    source = snakeOilPrivateKey;
  };

  environment.etc."pubkey.snakeoil" = {
    mode = "0600";
    #source = snakeOilPublicKey;
    text = snakeOilPublicKey;
  };

  services.slurm = {
    controlMachine = "server";
    nodeName = [ "node[1-${nbNodes}] CPUs=1 State=UNKNOWN" ];
    partitionName = [
      "DEFAULT Nodes=node[1-${nbNodes}] Default=YES State=UP DefaultTime=60"
      "bebida Nodes=node[1-${nbNodes}] Default=YES MaxTime=INFINITE"
    ];
    extraConfig = ''
      AccountingStorageHost=server
      AccountingStorageType=accounting_storage/slurmdbd
    '';
  };

  # Avoid error about xauth missing...
  services.openssh.settings.X11Forwarding = false;
  services.slurm.dbdserver = {
    dbdHost = "server";
    storagePassFile = "${passFile}";
  };
  services.mysql = {
    enable = true;
    package = pkgs.mariadb;
    initialScript = pkgs.writeText "mysql-init.sql" ''
      CREATE USER 'slurm'@'localhost' IDENTIFIED BY 'password123';
      GRANT ALL PRIVILEGES ON slurm_acct_db.* TO 'slurm'@'localhost';
    '';
    ensureDatabases = [ "slurm_acct_db" ];
    ensureUsers = [{
      ensurePermissions = { "slurm_acct_db.*" = "ALL PRIVILEGES"; };
      name = "slurm";
    }];
    settings.mysqld = {
      # recommendations from: https://slurm.schedmd.com/accounting.html#mysql-configuration
      innodb_buffer_pool_size = "1024M";
      innodb_log_file_size = "64M";
      innodb_lock_wait_timeout = 900;
    };
  };

  systemd.services.slurmdbd.serviceConfig = {
    Restart = "on-failure";
    RestartSec = 3;
  };
  systemd.services.slurmctld.serviceConfig = {
    Restart = "on-failure";
    RestartSec = 3;
  };

  systemd.tmpfiles.rules = [
    "f /etc/munge/munge.key 0400 munge munge - mungeverryweakkeybuteasytointegratoinatest"
  ];
  networking.firewall.enable = false;

}
