{ pkgs, ... }: {
  roles = {
    node = { pkgs, ... }:
    {
      imports = [ ../../modules/services/alumet/alumet.nix ]; 

      environment.systemPackages = with pkgs; [ vim influxdb2-cli ];

      services.alumet = {
        package = pkgs.callPackage ../../pkgs/alumet-cgroupv2/default.nix {};
        agent.enable = true;

        # Test config options
        influxDbPlugin = {
          enable = false;
        };
        raplPlugin = {
          enable = true;
        };
        csvPlugin.enable = true;
        socketControlPlugin.enable = true;
        oar3Plugin = {
          enable = true;
          path = "/sys/fs/cgroup/user.slice";
        };
      };

#      systemd.services.influxdb2.serviceConfig.RestartSec = 6000;
#      
#      services.influxdb2.enable = true;
#
#      services.influxdb2.provision = {
#        enable = true;
#        initialSetup = {
#          organization = "default";
#          bucket = "default";
#          passwordFile = pkgs.writeText "admin-pw" "password";
#          tokenFile = pkgs.writeText "admin-token" "token";
#        };
#        organizations.someorg = {
#          buckets.somebucket = {};
#          auths.sometoken = {
#            description = "some auth token";
#            readBuckets = ["somebucket"];
#            writeBuckets = ["somebucket"];
#          };
#        };
#        users.someuser.passwordFile = pkgs.writeText "tmp-pw" "password";
#      };
    };
  };
  testScript = ''
  '';
}
