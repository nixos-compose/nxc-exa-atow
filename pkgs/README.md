## Alumet packages :

There are differents packages for Alumet, because currently alumet does not support dynamic module.
So the packages provide different enabled modules.


- alumet : alumet app agent and app relay (default version for the module) plugins are rapl, perf, csv, influxdb
- alumet-cgroupv2 : enable cgroup v2 from the oar3 plugin but it does not work with influxdb plugin (see at the repo README.md for more info)
- alumet-without-csv : same as alumet package but disable csv plugin 
- alumet-without-rapl : same as alumet package but disable rapl plugin
