{
  lib,
  rustPlatform,
  fetchFromGitHub,
  pkg-config,
  protobuf,
  openssl,
  stdenv,
  darwin,
}:

rustPlatform.buildRustPackage rec {
  pname = "alumet";
  version = "alumet-relay-vm";

  src = fetchFromGitHub {
    owner = "YouriRigaud";
    repo = "alumet";
    rev = "d1dd2a75d631df54112c941ed78dabc94f507ac1";
    hash = "sha256-uffDUDnDcQFJTIkwCAPFDMBQ7kpnxu+KuhZEvxuHo/c=";
  };

  cargoLock = {
    lockFile = ./Cargo.lock;
    outputHashes = {
      "cbindgen-0.26.0" = "sha256-FXfp6eICzLHJ7s87A5XsoHoTP6d/wuf3S+1Bt+K5ET4=";
    };
  };

  postPatch = ''
    ln -s ${./Cargo.lock} Cargo.lock
  '';

  cargoBuildFlags = [ "-p" "app-agent" "-p" "app-relay-collector" "--features=client,server" ];

  doCheck = false;

  nativeBuildInputs = [
    pkg-config
    protobuf
    rustPlatform.bindgenHook
  ];

  buildInputs = [
    openssl
  ] ++ lib.optionals stdenv.isDarwin [
    darwin.apple_sdk.frameworks.CoreFoundation
    darwin.apple_sdk.frameworks.CoreServices
    darwin.apple_sdk.frameworks.Security
    darwin.apple_sdk.frameworks.SystemConfiguration
  ];

  meta = with lib; {
    description = "Adaptive toolbox and efficient measurement software for any device";
    homepage = "https://github.com/YouriRigaud/alumet/commit/5750e61ffe8c8df3d90c7bfd9783e9287cfd3838";
    license = licenses.eupl12;
    maintainers = with maintainers; [ ];
    mainProgram = "alumet";
  };
}
