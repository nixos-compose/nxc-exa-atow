{
  lib,
  rustPlatform,
  fetchFromGitHub,
  pkg-config,
  protobuf,
  openssl,
  stdenv,
  darwin,
}:

rustPlatform.buildRustPackage rec {
  pname = "alumet";
  version = "app-relay";

  src = fetchFromGitHub {
    owner = "YouriRigaud";
    repo = "alumet";
    rev = "434cabbe1a62c59fe682a2b9c0350da79c1742df";
    hash = "sha256-iTcNGug01fkLNZK64MPcwyLILr2FHY9d4p1gPlTxm98=";
  };

  cargoLock = {
    lockFile = ./Cargo.lock;
    outputHashes = {
      "cbindgen-0.26.0" = "sha256-FXfp6eICzLHJ7s87A5XsoHoTP6d/wuf3S+1Bt+K5ET4=";
    };
  };

  postPatch = ''
    ln -s ${./Cargo.lock} Cargo.lock
  '';

  cargoBuildFlags = [ "-p" "app-agent" "-p" "app-relay-collector" "--features=client,server" ];

  doCheck = false;

  nativeBuildInputs = [
    pkg-config
    protobuf
    rustPlatform.bindgenHook
  ];

  buildInputs = [
    openssl
  ] ++ lib.optionals stdenv.isDarwin [
    darwin.apple_sdk.frameworks.CoreFoundation
    darwin.apple_sdk.frameworks.CoreServices
    darwin.apple_sdk.frameworks.Security
    darwin.apple_sdk.frameworks.SystemConfiguration
  ];

  meta = with lib; {
    description = "Adaptive toolbox and efficient measurement software for any device";
    homepage = "https://github.com/YouriRigaud/alumet/commit/434cabbe1a62c59fe682a2b9c0350da79c1742df";
    license = licenses.eupl12;
    maintainers = with maintainers; [ ];
    mainProgram = "alumet";
  };
}
