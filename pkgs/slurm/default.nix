{ lib
, stdenv
, fetchFromGitHub
, perl
, munge
, python3
}:

stdenv.mkDerivation rec {
  pname = "slurm";
  version = "24-05-0-1";

  src = fetchFromGitHub {
    owner = "SchedMD";
    repo = "slurm";
    rev = "slurm-${version}";
    hash = "sha256-nPTgasNajSzSTv+64V7ykwFV5eZt300KMWQDlqNIz44=";
  };

  buildInputs = [ perl munge python3 ];

  configureFlags = [
    "--with-munge=${munge}"
  ];

  preConfigure =''
    patchShebangs ./doc/html/shtml2html.py
    patchShebangs ./doc/man/man2html.py
  '';

  meta = with lib; {
    description = "Slurm: A Highly Scalable Workload Manager";
    homepage = "https://github.com/SchedMD/slurm/tree/slurm-24-05-0-1";
    maintainers = with maintainers; [ ];
    mainProgram = "slurm";
    platforms = platforms.all;
  };
}
