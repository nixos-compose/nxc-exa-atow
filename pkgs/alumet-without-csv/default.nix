{ lib
, rustPlatform
, fetchFromGitHub
, pkg-config
, protobuf
, openssl
, stdenv
, darwin
}:

rustPlatform.buildRustPackage rec {
  pname = "alumet";
  version = "without-csv";

  src = fetchFromGitHub {
    owner = "YouriRigaud";
    repo = "alumet";
    rev = "41821917856591cd45c5fa40bf38526af6111327";
    hash = "sha256-eHz0Weo6K+x5DSRRLAdxImgqQIUW6D9bFMkqY4RnwKE=";
  };

  cargoLock = {
    lockFile = ./Cargo.lock;
    outputHashes = {
      "cbindgen-0.26.0" = "sha256-FXfp6eICzLHJ7s87A5XsoHoTP6d/wuf3S+1Bt+K5ET4=";
    };
  };

  postPatch = ''
    ln -s ${./Cargo.lock} Cargo.lock
  '';

  cargoBuildFlags = [ "-p" "app-agent" "-p" "app-relay-collector" "--features=client,server" ];

  doCheck = false;

  nativeBuildInputs = [
    pkg-config
    protobuf
    rustPlatform.bindgenHook
  ];

  buildInputs = [
    openssl
  ] ++ lib.optionals stdenv.isDarwin [
    darwin.apple_sdk.frameworks.CoreFoundation
    darwin.apple_sdk.frameworks.CoreServices
    darwin.apple_sdk.frameworks.Security
  ];

  meta = with lib; {
    description = "Adaptive toolbox and efficient measurement software for any device";
    homepage = "https://github.com/YouriRigaud/alumet/commit/41821917856591cd45c5fa40bf38526af6111327";
    license = licenses.eupl12;
    maintainers = with maintainers; [ ];
    mainProgram = "alumet";
  };
}
