{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.services.alumet;

  wrappers = "/run/wrappers/bin";

  alumetPkg = pkgs.callPackage ../../../pkgs/alumet/default.nix {};

  inherit (import ./alumet-conf.nix {
    pkgs = pkgs;
    lib = lib;
    cfg = cfg;
  })
  alumetAgentConf
  alumetClientConf
  alumetServerConf
  ;

  # A configuration file can be used with alumet
  # If this option is set to null (default) a config file is created with the options bellow
  # The config file depends of the binary used :
  #     - app-agent : alumet-config.toml
  #     - alumet-relay-client : alumet-agent.toml
  #     - alumet-relay-server : alumet-collector.toml
  configFile = mkOption {
    type = types.nullOr types.path;
    default = null;
    example = "/path/to/config-file.toml";
    description = ''
      The alumet configuration file can be added
      If this option is set to null (default) a config file is created with the options bellow
      The config file depends of the binary used:
          - app-agent : alumet-config.toml
          - alumet-relay-client : alumet-agent.toml
          - alumet-relay-server : alumet-collector.toml
    '';
  };

  maxUpdateIntervalOption = mkOption {
    type = types.str;
    default = "500ms";
  };

in

{
  options = {
    services.alumet = {

      # Permits to define your own package;
      package = mkOption {
        type = types.package;
        default = alumetPkg;
        description = "The alumet package to use";
      };

      # This is the path to the directory when Alumet should store its output files
      # By default this option is set to null, so Alumet will store its output to the working directory that is /etc/alumet
      outputFilePath = mkOption {
        type = types.nullOr types.path;
        default = null;
        example = "/desired/path/to/alumet-output";
        description = "Path to store the output of Alumet";
      };

      # Alumet app agent
      agent = {
        enable = mkEnableOption "alumet agent";
        configFile = configFile;
        maxUpdateInterval = maxUpdateIntervalOption;
      };

      # Alumet app relay client
      client = {
        enable = mkEnableOption "alumet relay client";
        configFile = configFile;
        maxUpdateInterval = maxUpdateIntervalOption;

        name = mkOption {
          type = types.nullOr types.str;
          default = null;
          description = "Name of the client node";
        };

        collectorUri = mkOption {
          type = types.str;
          default = "http://[::1]:50051";
        };
      };

      # Alumet app relay server
      server = {
        enable = mkEnableOption "alumet relay server";
        configFile = configFile;
        address = mkOption {
          type = types.str;
          default = "::1";
          description = ''
            Adress to listen on (by default it is ipv6 localhost ::1
            To listen all interfaces : 0.0.0.0 or ::
          '';
        };

        port = mkOption {
          type = types.ints.u16;
          default = 50051;
        };

      };

      #----------------------------------------------------------------#
      # The following options define the config of each plugins

      # Option to csv plugin
      csvPlugin = {
        enable = mkEnableOption "csvPlugin";

        outputPath = mkOption {
          type = types.str;
          default = "alumet-output.csv";
          description = " Output path of csv plugin";
        };

        forceFlush = mkOption {
          type = types.bool;
          default = true;
        };

        appendUnitToMetricName = mkOption {
          type = types.bool;
          default = true;
        };

        useUnitDisplayName = mkOption {
          type = types.bool;
          default = true;
        };

        csvDelimiter = mkOption {
          type = types.str;
          default = ";";
        };
      };

      # Option to set config of influxdb plugin
      influxDbPlugin = {
        enable = mkEnableOption "influxDbPlugin";

        host = mkOption {
          type = types.str;
          default = "http://localhost:8086";
          description = "The host of the influxdb";
        };

        token = mkOption {
          type = types.str;
          default = "token";
          description = "Token used by influxdb";
        };

        org = mkOption {
          type = types.str;
          default = "default";
          description = "Organization of influxdb";
        };

        bucket = mkOption {
          type = types.str;
          default = "default";
          description = "Bucket of influxdb";
        };
      };

      # Option to set config of rapl plugin
      raplPlugin = {
        enable = mkEnableOption "raplPlugin";

        pollInterval = mkOption {
          type = types.str;
          default = "1s";
        };

        flushInterval = mkOption {
          type = types.str;
          default = "5s";
        };

        noPerfEvents = mkOption {
          type = types.bool;
          default = false;
        };
      };

      # Option for the socket control plugin
      socketControlPlugin = {
        enable = mkEnableOption "socketControlPlugin";

        socketPath = mkOption {
          type = types.str;
          default = "alumet-control.sock";
        };
      };
      

      # Option for OAR3 plugin
      # For now this plugin permits to enable cgroup v2, probably have to modify this later
      oar3Plugin = {
        enable = mkEnableOption "oar3Plugin";

        path = mkOption {
          type = types.nullOr types.str;
          default = null;
          description = "Path to cgroup v2 to monitor";
          example = "/sys/fs/cgroup/user.slice/";
        };

        pollInterval = mkOption {
          type = types.str;
          default = "1s";
        };
      };

    };

  };

  config = mkIf (cfg.agent.enable || cfg.client.enable || cfg.server.enable) {
    assertions = [
      {
        assertion = cfg.package != null;
        message = "The package option for alumet must be set.";
      }
    ];

    # set perf_event_paranoid to 0, used by perf plugin
    boot.kernel.sysctl = mkIf (cfg.agent.enable || cfg.client.enable) {
      "kernel.perf_event_paranoid" = 0;
    };

    # setuid for alumet
    security.wrappers = {
      alumet-agent = {
        source = "${cfg.package}/bin/alumet-agent";
        owner = "alumet";
        group = "alumet";
        setuid = true;
        setgid = true;
        permissions = "u+rx,g+x";
      };

      alumet-client = {
        source = "${cfg.package}/bin/alumet-relay-client";
        owner = "alumet";
        group = "alumet";
        setuid = true;
        setgid = true;
        permissions = "u+rx,g+x";
      };

      alumet-server = {
        source = "${cfg.package}/bin/alumet-relay-server";
        owner = "alumet";
        group = "alumet";
        setuid = true;
        setgid = true;
        permissions = "u+rx,g+x";
      };
    };

    # alumet user declaration
    users.users.alumet = {
      description = "Alumet user";
      isSystemUser = true;
      group = "alumet";
      uid = 800;
    };

    users.groups.alumet.gid = 800;

    #################
    # Pre-alumet services

    systemd.services.alumet-conf-etc-dir = mkIf (cfg.agent.enable || cfg.server.enable) {
      description = "Change /etc/alumet permissions to allow alumet to create output file";
      wantedBy = [ "alumet-agent.service" "alumet-server.service" ];
      serviceConfig.Type = "oneshot";
      script = ''
        chmod 700 /etc/alumet
        chown alumet:alumet /etc/alumet
      '';
    };

    systemd.services.alumet-conf-rapl = mkIf (cfg.raplPlugin.enable) {
      description = "Allow read access to intel-rapl";
      wantedBy = [ "alumet-agent.service" "alumet-client.service" ];
      serviceConfig.Type = "oneshot";
      script = ''
        chmod a+r -R /sys/devices/virtual/powercap/intel-rapl
      '';
    };

    #################
    # Alumet agent

    environment.etc."alumet/alumet-config.toml" = mkIf cfg.agent.enable {
      mode = "0600";
      source = alumetAgentConf;
      user = "alumet";
      group = "alumet";
    };

    # This runs alumet-agent continuously during an experiment
    # then stop and produces the output file
    systemd.services.alumet-agent = mkIf cfg.agent.enable {
      description = "Alumet-agent service";
      wantedBy = [ "multi-user.target" ];
      after = [ "influxdb2.service" "alumet-conf-etc-dir.service" "alumet-conf-rapl.service" ];
      serviceConfig = {
        User = "alumet";
        Group = "alumet";
        Type = "simple";
        # Change the working directory to access the config file and the output file in /etc
        WorkingDirectory = /etc/alumet;
        # Start Alumet
        ExecStart = "${wrappers}/alumet-agent";
        Restart = "on-failure";
        RestartSec = 1;
      };
    };

    #######################
    # Alumet relay client

    environment.etc."alumet/alumet-agent.toml" = mkIf cfg.client.enable {
      mode = "0600";
      source = alumetClientConf;
      user = "alumet";
      group = "alumet";
    };

    systemd.services.alumet-client = mkIf cfg.client.enable {
      description = "Alumet relay client";
      wantedBy = [ "multi-user.target" ];
      after = [ "alumet-server.service" "alumet-conf-rapl.service" ];
      serviceConfig = {
        User = "alumet";
        Group = "alumet";
        Type = "simple";
        WorkingDirectory = /etc/alumet;
        ExecStart = "${wrappers}/alumet-client";
        Restart = "on-failure";
        RestartSec = 5;
      };
    };

    #########################
    # Alumet relay server

    environment.etc."alumet/alumet-collector.toml" = mkIf cfg.server.enable {
      mode = "0600";
      source = alumetServerConf;
      user = "alumet";
      group = "alumet";
    };

    systemd.services.alumet-server = mkIf cfg.server.enable {
      description = "Alumet relay server";
      wantedBy = [ "multi-user.target" ];
      after = [ "influxdb2.service" "alumet-conf-etc-dir.service" ];
      serviceConfig = {
        User = "alumet";
        Group = "alumet";
        Type = "simple";
        WorkingDirectory = /etc/alumet;
        ExecStart = "${wrappers}/alumet-server";
        Restart = "on-failure";
        RestartSec = 1;
      };
    };
  };
}
