{ pkgs, lib, cfg }:
with lib;
let
  maxUpdateInterval = if cfg.agent.enable then cfg.agent.maxUpdateInterval else cfg.client.maxUpdateInterval;

  baseConfString = ''
    max_update_interval = "${maxUpdateInterval}"

  '';

  raplConfString = ''
    [plugins.rapl]
    poll_interval = "${cfg.raplPlugin.pollInterval}"
    flush_interval = "${cfg.raplPlugin.flushInterval}"
    no_perf_events = ${boolToString cfg.raplPlugin.noPerfEvents}
    
  '';

  csvConfString = ''
    [plugins.csv]
    output_path = "${cfg.csvPlugin.outputPath}"
    force_flush = ${boolToString cfg.csvPlugin.forceFlush}
    append_unit_to_metric_name = ${boolToString cfg.csvPlugin.appendUnitToMetricName}
    use_unit_display_name = ${boolToString cfg.csvPlugin.useUnitDisplayName}
    csv_delimiter = "${cfg.csvPlugin.csvDelimiter}"

  '';

  perfConfString = ''
    [plugins.perf]
    hardware_events = ["REF_CPU_CYCLES", "CACHE_MISSES", "BRANCH_MISSES"]
    software_events = []
    cache_events = ["LL_READ_MISS"]
    
  '';

  influxDbConfString = ''
    [plugins.influxdb]
    host = "${cfg.influxDbPlugin.host}"
    token = "${cfg.influxDbPlugin.token}"
    org = "${cfg.influxDbPlugin.org}"
    bucket = "${cfg.influxDbPlugin.bucket}"
    attributes_as = "field"

  '';

  socketControlConfString = ''
    [plugins.socket-control]
    socket_path = "${cfg.socketControlPlugin.socketPath}"

  '';

  oar3ConfString = ''
    [plugins.OAR3]
    path = "${cfg.oar3Plugin.path}"
    poll_interval = "${cfg.oar3Plugin.pollInterval}"

  '';

  relayClientConfString = ''
    [plugins."plugin-relay:client"]
    client_name = "${cfg.client.name}"
    collector_uri = "${cfg.client.collectorUri}"

  '';

  portString = toString cfg.server.port;

  relayServerConfString = ''
    [plugins."plugin-relay:server"]
    address = "${cfg.server.address}"
    port = ${portString}

  '';

  alumetAgentConf = if cfg.agent.configFile != null then 
    cfg.agent.configFile
  else
    pkgs.writeText "alumet-config.toml" (
      baseConfString +
      (if cfg.raplPlugin.enable then raplConfString else "") +
      (if cfg.csvPlugin.enable then csvConfString else "") +
      (if cfg.socketControlPlugin.enable then socketControlConfString else "") +
      perfConfString +
      (if cfg.oar3Plugin.enable then oar3ConfString else "") +
      (if cfg.influxDbPlugin.enable then influxDbConfString else "")
    );

  alumetClientConf = if cfg.client.configFile != null then 
    cfg.client.configFile
  else
    pkgs.writeText "alumet-agent.toml" (
      baseConfString +
      relayClientConfString +
      (if cfg.raplPlugin.enable then raplConfString else "") +
      (if cfg.oar3Plugin.enable then oar3ConfString else "")
    );

  alumetServerConf = if cfg.server.configFile != null then 
    cfg.server.configFile
  else
    pkgs.writeText "alumet-collector.toml" (
      relayServerConfString +
      (if cfg.csvPlugin.enable then csvConfString else "") +
      (if cfg.influxDbPlugin.enable then influxDbConfString else "")
    );

in {

  inherit alumetAgentConf;
  inherit alumetClientConf;
  inherit alumetServerConf;
  
}



