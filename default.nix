{ pkgs ? import <nixpkgs> {} }:

rec {

#  slurm = pkgs.callPackage ./pkgs/slurm { };

  alumet = pkgs.callPackage ./pkgs/alumet { };

  alumet-cgroupv2 = pkgs.callPackage ./pkgs/alumet-cgroupv2 { };

  alumet-without-csv = pkgs.callPackage ./pkgs/alumet-without-csv { };

  alumet-without-rapl = pkgs.callPackage ./pkgs/alumet-without-rapl { };

}
