# Report about this repo

It contains a module for the alumet service, different packages for alumet (with different plugins enabled) and some compositions for nixos-compose.

## Packages :

In this directory there are 4 different packages for alumet.
Each package can run alumet app-agent and app-relay client or server.
For the moment (1st August) Alumet does not support dynamic plugins, to use or not a plugin we should rebuild the code.
So these packages provide different plugin configurations for alumet. See these configs in pkgs/README.md

These packages were created from a fork of [alumet](https://github.com/YouriRigaud/alumet) because I needed to manually enable or disable plugins.
I used fetchFromGitHub with the hash of the commit of the wanted version. Tags on GitHub point to these commits.  
For the moment, to add or remove a plugin from the source, we need to add or remove the plugin from the dependencies of the package app-agent and/or app-relay-collector.
Additionally, we need to add or remove the plugin in file app-agent/src/main.rs (and/or app-relay-collector/src/main\_{client,server}.rs) in the line `let plugins = static_plugins![...]`.

> [!NOTE]
> For alumet-cgroupv2, this version has a bug. The cgroup plugin do not work with influxdb.
> It is fixed now in the [feature/oar3\_plugin](https://github.com/alumet-dev/alumet/tree/feature/oar3_plugin) branch, but I did not change it yet in the alumet package.

> [!TIP]
> When packaging Alumet, add `doCheck = false;` to reduce the build time and `cargoBuildFlags = [ "-p" "app-agent" "-p" "app-relay-collector" "--features=client,server" ];` to build the three different binaries of Alumet.

We can build these packages with `nix-build` or with `nix build .#packages.alumet-version` with flake in the git root directory.


## Module :

It contains a single service for Alumet.

`alumet.nix` defines the options and the config of the module. `alumet-conf.nix` creates the differents configuration files for alumet.  

The options define the package used, outputFilePath (if wanted) and which binary of alumet will be used (agent for the agent app, and client or server for the relay app).
So it is mandatory to have only one of these three options enabled.
Options also define the configuration of each plugins. For the moment there is no documentation of Alumet, but we can see the config of each plugins in the source code of Alumet.
We can also look into the auto-generated config file by Alumet.  

For the config, there were several problems to solve.  
Firstly we wanted to use Alumet in relay mode, so several clients run Alumet and send the data to the Alumet server.
An issue is to start the client service after the server service. To solve this I set restart on-failure for the service with a restartSec of 5s.  
Then we wanted Alumet to work in user mode for safety reasons.
So I created a system user `alumet` that will start the services. The issue is that some plugins need capabilities to work (e.g. rapl plugin) and need access to some system files.
So I created a oneshot service (`alumet-conf-rapl`) that adds permission needed by the rapl plugin. I also created wrappers for the different binaries that allow the user `alumet` to execute the binaries with root privileges by the setuid.
I believe this should be temporary because for safety reason it is better to set the right capabilities.  
Alumet uses the config file that is in its working directory, so I decided to create a directory `/etc/alumet` that only alumet user has access, in this directory there is the configuration file and it is the working space of the service.  

> [!NOTE]
> To use the cgroupv2 we have to use the plugin OAR3, so `oar3Plugin` is the name of the option to enable. I do not know if the plugin will still be called OAR3. I think it is better to ask to Guillaume Raffin.


## Composition :

This contains two compositions to test Alumet.  

`alumet-relay` uses the relay mode with one client and the server. It uses the package without csv, so there is the rapl plugin for the client, and there is influxdb plugin for the server.
We can connect to influxdb with influxcli (but first we have to configure the cli setup to access to the db) or we can access by the influx dashboard on a web browser.  
To do that, we have to create a ssh tunnel with the following command `ssh user@access.grid5000.fr -L 8086:node.site.grid5000.fr:8086` selecting your user and the right node (I use ifconfig to see the ip address of the server node, so I know which node it is).
Then launch a web browser and go on `http://localhost:8086`. To connect, the user is `admin` and password is `password`.  

> [!NOTE]
> Maybe you will have to change the config of the dashboard to see the data. So click on the `d` button on the left panel, then `switch organization` and select `default`.
> Then you can see the data in `data explorer`.

`alumet-cgroupv2` uses the package cgroupv2, it is just to test cgroup, so there is only one node with alumet agent and a csv output.


## Next things to do :

- When dynamic plugin will be available, package the new release.
- Package the fix of influxdb for the cgroupv2.
- Look into using capabilities instead of setuid ?
