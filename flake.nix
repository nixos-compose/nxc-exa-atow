{
  description = "Nxc exa atow repository";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs?ref=24.05";
  };

  outputs = { self, nixpkgs }:
  let
    system = "x86_64-linux";
    pkgs = import nixpkgs { inherit system; };
    myPackages = import ./default.nix { inherit pkgs; };
  in
  {
    packages = myPackages;
    nixosModules = import ./modules;

  };
}
